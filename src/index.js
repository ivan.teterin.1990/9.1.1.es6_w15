'use strict';

/**
 *
 * @param str: {String}
 * @returns {Boolean}
 */
const isValidJSON = (str) => {
    // Your implementation here
try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
    //throw new Error('Task not implemented');
};

//console.log(isValidJSON('{"a": 2}')); // result: true;
//console.log(isValidJSON('{"a: 2')); // result: false;

/**
 *
 * @param params: {Object}
 * @returns {String}
 */
const greeting = (params) => {
    // Your implementation here
return `Hello, my name is ${params.name} ${params.surname} and I am ${params.age} years old.`;
    //throw new Error('Task not implemented');
};

//console.log(greeting({name: 'Bill', surname: 'Jacobson', age: 33})); // result: Hello, my name is Bill Jacobson and I am 33 years old.
//console.log(greeting({name: 'Jim', surname: 'Power', age: 11})); // result: Hello, my name is Jim Power and I am 11 years old.

module.exports = {
    isValidJSON,
    greeting
};